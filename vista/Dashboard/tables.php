<!DOCTYPE html>

<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="Dashboard">
        <meta name="keyword" content="Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">

        <title>BIENVENIDO::DATA WAREHOUSE</title>

        <!-- Bootstrap core CSS -->
        <link href="<?php echo DIRECCION; ?>/vista/Dashboard/assets/css/bootstrap.css" rel="stylesheet">
        <!--external css-->
        <link href="<?php echo DIRECCION; ?>/vista/Dashboard/assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
        <link rel="stylesheet" type="text/css" href="<?php echo DIRECCION; ?>/vista/Dashboard/assets/css/zabuto_calendar.css">
        <link rel="stylesheet" type="text/css" href="<?php echo DIRECCION; ?>/vista/Dashboard/assets/js/gritter/css/jquery.gritter.css" />
        <link rel="stylesheet" type="text/css" href="<?php echo DIRECCION; ?>/vista/Dashboard/assets/lineicons/style.css">    
        
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>
        <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/themes/smoothness/jquery-ui.css">
 
        <!-- Custom styles for this template -->
        <link href="<?php echo DIRECCION; ?>/vista/Dashboard/assets/css/style.css" rel="stylesheet">
        <link href="<?php echo DIRECCION; ?>/vista/Dashboard/assets/css/style-responsive.css" rel="stylesheet">

        <script src="<?php echo DIRECCION; ?>/vista/Dashboard/assets/js/chart-master/Chart.js"></script>

        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
        <style>
            #sortable1, #sortable2 {
              border: 1px solid #eee;
              width: 142px;
              min-height: 20px;
              list-style-type: none;
              margin: 0;
              padding: 5px 0 0 0;
              float: left;
              margin-right: 10px;
            }
            #sortable1 li, #sortable2 li {
              margin: 0 5px 5px 5px;
              padding: 5px;
              font-size: 1.2em;
              width: 120px;
            }
        </style>
    </head>

    <body>
        <section id="container" >
            <header class="header black-bg">
                <div class="sidebar-toggle-box">
                    <div class="fa fa-bars tooltips" data-placement="right" data-original-title="Toggle Navigation"></div>
                </div>
                <!--logo start-->
                <a class="logo"><b>DATA WAREHOUSE</b></a>
                <!--logo end-->
                <div class="top-menu">
                    <ul class="nav pull-right top-menu">
                        <li><a class="logout" href="<?php echo DIRECCION; ?>/usuario_Acceso/salir">Salir</a></li>
                    </ul>
                </div>
            </header>
            <aside>
                <div id="sidebar"  class="nav-collapse ">
                    <!-- sidebar menu start-->
                    <ul class="sidebar-menu" id="nav-accordion">

                        <p class="centered"><img src="<?php echo DIRECCION; ?>/vista/Dashboard/assets/img/db.jpg" class="img-circle" width="60"></p>
                        <h5 class="centered"><?php echo  $nombre.' '. $apellido  ?></h5>

                        <li class="mt">
                            <a class="active">
                                <i class="fa fa-dashboard"></i>
                                <span>Dashboard::Databases</span>
                            </a>
                        </li>
                        <?php foreach ($databases as $database){?>
                            <li class="sub-menu">
                                <a href="<?php echo DIRECCION; ?>/usuario_Principal/tables/<?php echo $database->obtenerNombre();?>">
                                    <i class="fa fa-database"></i>
                                    <?php echo $database->obtenerNombre();?>
                                </a>
                            </li>
                        <?php }?>
                    </ul>
                </div>
            </aside>
            <section id="main-content">
                <section class="wrapper">
                    <div class="row">
                        
                        <div class="col-lg-9 main-chart">
                            <div class="container">
                                <div class="row mtbox" id="tablas">
                                    <div class="col-xs-6">
                                        <?php foreach ($tables as $table){?>
                                            <div class="col-md-2 col-sm-2 box0">
                                                <a href="<?php echo DIRECCION; ?>/usuario_Principal/tables/">
                                                    <div class="box1">
                                                        <span class="fa fa-table" ></span>
                                                        <a href="#" onclick="getAttributes('<?php echo $table->obtenerNombre();?>')"><?php echo $table->obtenerNombre();?></a>
                                                    </div>
                                                </a>
                                            </div>                   
                                        <?php }?>

                                    </div><!-- /row mt -->
                                
                                <div class="col-xs-6" id="atributos">
                                    
                                </div>
                                </div>
                            </div>  
                        </div>
                    </div>
                </section>
            </section>
        </section>

<!--        <footer class="site-footer">
            <div class="text-center">
                2016 - DATA WAREHOUSE::UCQ
                <a href="#" class="go-top">
                    <i class="fa fa-angle-up"></i>
                </a>
            </div>
        </footer>-->
        <!-- js placed at the end of the document so the pages load faster -->
        <script src="<?php echo DIRECCION; ?>/vista/Dashboard/assets/js/jquery.js"></script>
        <script src="<?php echo DIRECCION; ?>/vista/Dashboard/assets/js/jquery-1.8.3.min.js"></script>
        <script src="<?php echo DIRECCION; ?>/vista/Dashboard/assets/js/bootstrap.min.js"></script>
        <script class="include" type="text/javascript" src="<?php echo DIRECCION; ?>/vista/Dashboard/assets/js/jquery.dcjqaccordion.2.7.js"></script>
        <script src="<?php echo DIRECCION; ?>/vista/Dashboard/assets/js/jquery.scrollTo.min.js"></script>
        <script src="<?php echo DIRECCION; ?>/vista/Dashboard/assets/js/jquery.nicescroll.js" type="text/javascript"></script>
        <script src="<?php echo DIRECCION; ?>/vista/Dashboard/assets/js/jquery.sparkline.js"></script>


        <!--common script for all pages-->
        <script src="<?php echo DIRECCION; ?>/vista/Dashboard/assets/js/common-scripts.js"></script>

        <script type="text/javascript" src="<?php echo DIRECCION; ?>/vista/Dashboard/assets/js/gritter/js/jquery.gritter.js"></script>
        <script type="text/javascript" src="<?php echo DIRECCION; ?>/vista/Dashboard/assets/js/gritter-conf.js"></script>

        <!--script for this page-->
        <script src="<?php echo DIRECCION; ?>/vista/Dashboard/assets/js/sparkline-chart.js"></script>    
        <script src="<?php echo DIRECCION; ?>/vista/Dashboard/assets/js/zabuto_calendar.js"></script>	
    
	
	
        <script>

            $(function(){
              $( "#sortable1, #sortable2" ).sortable({
                connectWith: ".connectedSortable"
              }).disableSelection();
            });

            function getAttributes(name) {
                $.ajax({
                    type: "POST",
                    data: {name: name},
                    dataType: "json",
                    url: '<?php echo DIRECCION . '/json/json'; ?>',
                    success: function(data) {
                        $('#atributos').empty();
                        $.each(data, function(index, value) {
                            $('#atributos').append("<span>" + value + "</span>");
                        });
                    }
                });
            }
            
            $(document).ready(function () {
                $("#date-popover").popover({html: true, trigger: "manual"});
                $("#date-popover").hide();
                $("#date-popover").click(function (e) {
                    $(this).hide();
                });

                $("#my-calendar").zabuto_calendar({
                    action: function () {
                        return myDateFunction(this.id, false);
                    },
                    action_nav: function () {
                        return myNavFunction(this.id);
                    },
                    ajax: {
                        url: "show_data.php?action=1",
                        modal: true
                    },
                    legend: [
                        {type: "text", label: "Special event", badge: "00"},
                        {type: "block", label: "Regular event", }
                    ]
                });
            });


            function myNavFunction(id) {
                $("#date-popover").hide();
                var nav = $("#" + id).data("navigation");
                var to = $("#" + id).data("to");
                console.log('nav ' + nav + ' to: ' + to.month + '/' + to.year);
            }
        </script>
    </body>
</html>
