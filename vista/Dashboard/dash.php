<!DOCTYPE html>

<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="Dashboard">
        <meta name="keyword" content="Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">

        <title>BIENVENIDO::DATA WAREHOUSE</title>

        <!-- Bootstrap core CSS -->
        <link href="<?php echo DIRECCION; ?>/vista/Dashboard/assets/css/bootstrap.css" rel="stylesheet">
        <!--external css-->
        <link href="<?php echo DIRECCION; ?>/vista/Dashboard/assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
        <link rel="stylesheet" type="text/css" href="<?php echo DIRECCION; ?>/vista/Dashboard/assets/css/zabuto_calendar.css">
        <link rel="stylesheet" type="text/css" href="<?php echo DIRECCION; ?>/vista/Dashboard/assets/js/gritter/css/jquery.gritter.css" />
        <link rel="stylesheet" type="text/css" href="<?php echo DIRECCION; ?>/vista/Dashboard/assets/lineicons/style.css">    
        
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>
        <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/themes/smoothness/jquery-ui.css">
 
        <!-- Custom styles for this template -->
        <link href="<?php echo DIRECCION; ?>/vista/Dashboard/assets/css/style.css" rel="stylesheet">
        <link href="<?php echo DIRECCION; ?>/vista/Dashboard/assets/css/style-responsive.css" rel="stylesheet">

        <script src="<?php echo DIRECCION; ?>/vista/Dashboard/assets/js/chart-master/Chart.js"></script>
        <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
        <script src="//code.jquery.com/jquery-1.10.2.js"></script>
        <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
              <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
        <style>
            #sortable1, #sortable2 {
              border: 1px solid #eee;
              width: 142px;
              min-height: 20px;
              list-style-type: none;
              margin: 0;
              padding: 5px 0 0 0;
              float: left;
              margin-right: 10px;
            }
            #sortable1 li, #sortable2 li {
              margin: 0 5px 5px 5px;
              padding: 5px;
              font-size: 1.2em;
              width: 120px;
            }
        </style>
        
        <script>
             $(function() {
                $("sortable1, #sortable2").sortable({
                  connectWith: ".connectedSortable"
                });
            });
        </script>
    </head>

    <body>
        <div class="top-content">
            <div class="inner-bg">
                <header class="header black-bg">
                    <div class="sidebar-toggle-box">
                        <div class="fa fa-bars tooltips" data-placement="right" data-original-title="Ver Databases"></div>
                    </div>
                    <a class="logo"><b>DATA WAREHOUSE</b><?php echo "  :: ". $nombre.' '. $apellido  ?></a>                            
                    <div class="top-menu">
                        <ul class="nav pull-right top-menu">
                            <li><a class="logout" href="<?php echo DIRECCION; ?>/usuario_Acceso/salir">Salir</a></li>
                        </ul>
                    </div>
                </header>

                <aside>
                    <div id="sidebar"  class="nav-collapse ">
                        <ul class="sidebar-menu" id="nav-accordion">
                            <p class="centered"><img src="<?php echo DIRECCION; ?>/vista/Dashboard/assets/img/db.jpg" class="img-circle" width="60"></p>
                            <h5 class="centered"><?php echo  $nombre.' '. $apellido  ?></h5>
                            <li class="mt">
                                <a class="active">
                                    <i class="fa fa-dashboard"></i>
                                    <span>Dashboard::Databases</span>
                                </a>
                            </li>
                            <?php foreach ($databases as $database){?>
                                <li class="sub-menu">
                                    <a href="#" onclick="getTables('<?php echo $database->obtenerNombre();?>')">
                                        <i class="fa fa-database"></i>
                                        <?php echo $database->obtenerNombre();?>
                                    </a>
                                </li>
                            <?php }?>
                        </ul>
                    </div>
                </aside>
                
                <section id="main-content">
                    <section class="wrapper"> 
                        <div class="container-fluid">
                            <div class="row">
                                
                                <div style="width: 20em" class="col-xs-4" id="tables">
                                    
                                </div>
                       
                                <div style="width: 20em" class="col-xs-4" id="attributes">
         
                                </div>
                            
                                <div style="width: 20em" class="col-xs-4" id="container">
                                    
                                </div>
                            </div> 
                            
                        </div>
                    </section>
                </section>
            </div>
        </div>
        <!-- js placed at the end of the document so the pages load faster -->
        <script src="<?php echo DIRECCION; ?>/vista/Dashboard/assets/js/jquery.js"></script>
        <script src="<?php echo DIRECCION; ?>/vista/Dashboard/assets/js/jquery-1.8.3.min.js"></script>
        <script src="<?php echo DIRECCION; ?>/vista/Dashboard/assets/js/bootstrap.min.js"></script>
        <script class="include" type="text/javascript" src="<?php echo DIRECCION; ?>/vista/Dashboard/assets/js/jquery.dcjqaccordion.2.7.js"></script>
        <script src="<?php echo DIRECCION; ?>/vista/Dashboard/assets/js/jquery.scrollTo.min.js"></script>
        <script src="<?php echo DIRECCION; ?>/vista/Dashboard/assets/js/jquery.nicescroll.js" type="text/javascript"></script>
        <script src="<?php echo DIRECCION; ?>/vista/Dashboard/assets/js/jquery.sparkline.js"></script>
        <!--common script for all pages-->
        <script src="<?php echo DIRECCION; ?>/vista/Dashboard/assets/js/common-scripts.js"></script>
        <script type="text/javascript" src="<?php echo DIRECCION; ?>/vista/Dashboard/assets/js/gritter/js/jquery.gritter.js"></script>
        <script type="text/javascript" src="<?php echo DIRECCION; ?>/vista/Dashboard/assets/js/gritter-conf.js"></script>
        <!--script for this page-->
        <script src="<?php echo DIRECCION; ?>/vista/Dashboard/assets/js/sparkline-chart.js"></script>    
        <script src="<?php echo DIRECCION; ?>/vista/Dashboard/assets/js/zabuto_calendar.js"></script>	
    
        <script type="text/javascript">
            function getTables(nameDb) {
                
                $.ajax({
                    type: "POST",
                    data: {nameDb: nameDb},
                    dataType: "json",
                    url: '<?php echo DIRECCION . '/json/getTables'; ?>',
                    success: function(data) {
                        $('#tables').empty();
                        $('#attributes').empty();
                        $('#container').empty();
                        $.each(data, function(index, value) {
                            $('#tables').append('<button class="btn-group-vertical btn-lg" style="width: 12em" type="button" href="#" onclick="getAttributes('+"'"+value+","+nameDb+"'"+')">'+value+'</button><br/>');
                        });   
                    }
                });
            }
            
            function getAttributes(table) {
                var datos = table.split(",");
                var tableName = datos[0];
                var db = datos[1];
                
                $.ajax({
                    type: "POST",
                    data: {tableName: tableName,db:db },
                    dataType: "json",
                    url: '<?php echo DIRECCION . '/json/getAttributes'; ?>',
                   success: function(data) {
                        $('#attributes').empty();
                        $.each(data, function(index, value) {
                            $('#attributes').append('<button id="'+value+'" class="btn-group-vertical btn-lg" style="width: 12em" type="button" onclick="addAttribute(this.id);">'+value+'</button><br/>');
                        });   
                    }         
                });
            }            
            
            function addAttribute(attr){
              
                $('#container').append('<button id="'+attr+'" class="btn-group-vertical btn-lg" style="width: 12em" type="button">'+attr+'</button><br/>');
               
            }
            
            function addAttribute(attr){
              
                $('#container').append('<button id="'+attr+'" class="btn-group-vertical btn-lg" style="width: 12em" type="button">'+attr+'</button><br/>');
            }
        </script>
    </body>
</html>
