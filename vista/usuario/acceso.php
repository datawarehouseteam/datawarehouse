
<!DOCTYPE html>
<html lang="es">
    <head>
    
        <meta charset="UTF-8"/>
        
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        
        <title>DATA WAREHOUSE</title>
                
        <link rel="stylesheet" type="text/javascript" href="http://fonts.googleapis.com/css?family=Roboto:400,100,300,500">
        <link rel="stylesheet" type="text/css" href="<?php echo DIRECCION; ?>/recursos/assets/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="<?php echo DIRECCION; ?>/recursos/assets/font-awesome/css/font-awesome.min.css">
        <link rel="stylesheet" type="text/css" href="<?php echo DIRECCION; ?>/recursos/assets/css/form-elements.css">
        <link rel="stylesheet" type="text/css" href="<?php echo DIRECCION; ?>/recursos/assets/css/style.css">

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->

        <!-- Favicon and touch icons -->
        <link rel="shortcut icon" href="<?php echo DIRECCION; ?>/recursos/assets/ico/database.png">
        <link rel="apple-touch-icon-precomposed"  sizes="144x144" href="<?php echo DIRECCION; ?>/recursos/assets/ico/apple-touch-icon-144-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo DIRECCION; ?>/recursos/assets/ico/apple-touch-icon-114-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo DIRECCION; ?>/recursos/assets/ico/apple-touch-icon-72-precomposed.png">
        <link rel="apple-touch-icon-precomposed" href="<?php echo DIRECCION; ?>/recursos/assets/ico/apple-touch-icon-57-precomposed.png">
    </head>

    <body>
        <!-- Top content -->
        <div class="top-content">
            <div class="inner-bg">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-8 col-sm-offset-2 text">
                            <h1><strong>PROYECTO DATA WAREHOUSE</strong> Optativa III</h1>
                        </div>
                    </div>           

                    <div class="row">
                        <div class="col-sm-6 col-sm-offset-3 form-box">
                            <div class="form-top">
                                <div class="form-top-left">
                                    <h3>Acceso al Sitio</h3>
                                    <p>Ingresa tus datos.</p>
                                </div>
                                <div class="form-top-right">
                                    <i class="fa fa-key"></i>
                                </div>
                            </div>
                            <div class="form-bottom">
                                <form id="acceso" class="login-form" method="post" action="<?php echo DIRECCION ?>/usuario_Acceso/entrar">
                                    <div class="form-group">
                                        <label class="sr-only" for="nombre">Usuario</label>
                                        <input type="text" name="nombre" placeholder="Usuario" class="form-username form-control" id="nombre">
                                    </div>
                                    <div class="form-group">
                                            <label class="sr-only" for="clave">Password</label>
                                            <input type="password" name="clave" placeholder="Contraseña" class="form-password form-control" id="clave">
                                    </div>
                                    <button type="submit" class="btn">Ingresar</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div> 
        </div>
    <!-- Javascript -->
        <script src="<?php echo DIRECCION; ?>/recursos/assets/js/jquery-1.11.1.min.js"></script>
        <script src="<?php echo DIRECCION; ?>/recursos/assets/bootstrap/js/bootstrap.min.js"></script>
        <script src="<?php echo DIRECCION; ?>/recursos/assets/js/jquery.backstretch.min.js"></script>
        <script src="<?php echo DIRECCION; ?>/recursos/assets/js/scripts.js"></script>
    </body>
    
</html>
