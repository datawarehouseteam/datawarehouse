<?php
namespace controlador;

use sistema\Controlador;
use \sistema\Peticion;
use modelo\com\Databases;
use sistema\Vista;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Json
 *
 * @author Eduardo
 */
class Json extends Controlador {
    //put your code here
    
    public function __construct(Peticion $peticion) {
        parent::__construct($peticion);
    }
    
    
    public function getTables() {
        $nameDb = $this->obtenerPeticion()->obtenerPost("nameDb");
        
        $tablesArray = array();
        $tables = Databases::obtenerTablasPorDb($nameDb);
        $respuesta = $this->obtenerRespuesta();
        foreach ($tables as $table) {
            $tablesArray[] = $table->obtenerNombre();
        }
        $respuesta->agregarSalida(json_encode($tablesArray));
        return $respuesta;
    }
    
    public function getAttributes() {
        $nameTable = $this->obtenerPeticion()->obtenerPost('tableName');
        $dbName = $this->obtenerPeticion()->obtenerPost('db');
        $attributesArray = array();
        $attributes = Databases::obtenerAtributos($dbName, $nameTable);
        $respuesta = $this->obtenerRespuesta();
        foreach ($attributes as $attribute) {
            $attributesArray[] = $attribute->obtenerNombre();
        }
        $respuesta->agregarSalida(json_encode($attributesArray));
        return $respuesta;
    }

}
