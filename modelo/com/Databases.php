<?php
namespace modelo\com;

use sistema\BD;
/**
 * Description of Usuario
 *
 * @author javier.hernandez
 */

class Databases {
    
    private $nombre;
    
    
    public function __construct($nombre) {
        
        $this->nombre = $nombre;
    } 
    
    /**
     * 
     * @return \Doctrine\DBAL\Connection
     */
    private static function db() {
        return BD::instancia()->obtenerConexion("bd1");
    }
    
    public static function obtenerDbPorUsuario($usuario) {
        $databases = array();
        $sql = "SELECT u.User,Db AS nombre FROM mysql.user u,mysql.db d WHERE u.User=d.User AND u.User = ?";
        $stmt = self::db()->prepare($sql);
        $stmt->bindValue(1, $usuario);
        $stmt->execute();       
        
        while ($dato = $stmt->fetch()) {          
            $databases[] = new Databases($dato['nombre']);
        }
        return $databases;
    }
    
    public static function obtenerTablasPorDb($db) {
        
        $tables = array();
        
        $sql = "SELECT DISTINCT table_name AS nombre FROM information_schema.columns WHERE  table_schema = ?";
                
        $stmt = self::db()->prepare($sql);
        $stmt->bindValue(1, $db);
        $stmt->execute();   
        while ($dato = $stmt->fetch()) { 
            $tables[]  = new Databases($dato['nombre']);
        }
        return $tables;
    }
    
    public static function obtenerAtributos($db, $table){
        $atributos = array();
        
        $sql = "SELECT column_name AS nombre FROM information_schema.columns WHERE table_name = ? AND table_schema = ?";
        $stmt = self::db()->prepare($sql);
        $stmt->bindValue(1, $table);
        $stmt->bindValue(2, $db);
        $stmt->execute();
        
        while ($dato = $stmt->fetch()) { 
            $atributos[] = new Databases($dato['nombre']);
        }
        
        return $atributos;
        
    }

    public function obtenerNombre() {
        return $this->nombre;
    }
}
